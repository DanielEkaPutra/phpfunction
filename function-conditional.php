<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>

    <?php
    
        echo "<h3>Soal No 1</h3>";
        function greetings($string)
        {
            echo "Hallo $string, Selamat Datang di Sanbercode! <br>";
        }
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<br>";
        echo "<h3>Soal No 2</h3>";
        function reverse($kata)
        {
            $panjang = strlen($kata);
            $tampung = "";
            for ($i=($panjang-1); $i >= 0 ; $i--) { 
                $tampung .=$kata[$i];
            }
            return $tampung;
        }

        function reverseString($kata2)
        {
            $string = reverse($kata2);
            echo $string . "<br>";
        }

        reverseString("abduh");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");
        echo "<br>";

        echo "<h3>Soal No 3</h3>";
        function palindrome($kata3)
        {
            $balik = reverse($kata3);
            if($kata3 == $balik)
            {
                echo $kata3 ." => True <br>";
            }else
            {
                echo $kata3 ." => False <br>"; 
            }

        }
        palindrome("civic") ; // true
        palindrome("nababan") ; // true
        palindrome("jambaban"); // false
        palindrome("racecar"); // true

        echo "<h3>Soal No 4</h3>";
        function tentukan_nilai($angka)
        {
            if($angka > 85 && $angka < 100)
            {
                return "Nilai " . $angka ." => Sangat Baik <br>";
            } else if($angka >=70 && $angka < 85)
            {
                return "Nilai " . $angka ." => Baik <br>";
            } else if($angka >=60 && $angka < 70)
            {
                return "Nilai " . $angka ." => Cukup <br>";
            }else
            {
                return "Nilai " . $angka ." => Kurang <br>";
            }
        }
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

    ?>
</body>
</html>